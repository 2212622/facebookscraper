# FacebookScraper



## Description

This is a Facebook scraper dedicated to Ali+taptap project.
## Installation

Before running the program, you should download all dependencies. You have 2 options:

- [ ] Go to [FacebookScaper.ipynb](FacebookScraper/FacebookScraper.ipynb) or
- [ ] Run this code on your python terminal:

```
!pip install browser_cookie3
!pip install requests_html
!pip install pyppeteer
!pip install nest-asyncio
!pip install facebook-scraper
```

## How to use
you can modify the file [FacebookScaper.ipynb](FacebookScraper/FacebookScraperRunner.py)
