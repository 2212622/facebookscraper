import subprocess
from flask import Flask
import Facebook_Scraper as fs
import multiprocessing
import sched
import time

app = Flask(__name__)
scheduler = sched.scheduler(time.time, time.sleep)

def execute_facebook_scraper():
    keywords = ["Baguio Fire", "Baguio Typhoon", "Baguio Earthquake", "Baguio Landslide",
                "Baguio Rescue", "Baguio Donation"]
    time = 4
    csv_file = "Scraped_Data_Latest.csv"
    fs.search(keywords, time, csv_file)
    scheduler.enter(900, 1, execute_facebook_scraper)  # Schedule next execution after 15 minutes (900 seconds)
    return 'Python script executed!'

@app.route('/')
def home():
    return 'Test Website'

if __name__ == '__main__':
    scraper_process = multiprocessing.Process(target=execute_facebook_scraper)
    scraper_process.start()
    scheduler.enter(0, 1, execute_facebook_scraper)  # Schedule the initial execution
    scheduler.run()

    app.run()
