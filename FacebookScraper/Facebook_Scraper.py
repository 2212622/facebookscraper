# Use this code instead
import asyncio
import browser_cookie3
from bs4 import BeautifulSoup
import nest_asyncio
from pyppeteer import launch
import csv

cookies_for_facebook_scraper = browser_cookie3.load(domain_name='.facebook.com')
post_text = []


async def search_fb(keywords, time, csv_file):
    # creates browser. Turn headless to False if you want to see chromium in action
    browser = await launch(headless=True, args=['--no-sandbox'], userDataDir='./my_temp_dir')
    page = await browser.newPage()

    headers = {
        'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7',
        'Accept-Encoding': 'gzip, deflate, br',
        'Accept-Language': 'en-US,en;q=0.9',
        'Host': 'httpbin.org',
        'Sec-Ch-Ua': '"Not.A/Brand";v="8", "Chromium";v="114", "Google Chrome";v="114"',
        'Sec-Ch-Ua-Mobile': '?0',
        'Sec-Ch-Ua-Platform': '"Windows"',
        'Sec-Fetch-Dest': 'document',
        'Sec-Fetch-Mode': 'navigate',
        'Sec-Fetch-Site': 'none',
        'Sec-Fetch-User': '?1',
        'Upgrade-Insecure-Requests': '1',
        'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36',
        'X-Amzn-Trace-Id': 'Root=1-64b52d69-3c414fdb7349cf56227419e3'
    };

    await page.setExtraHTTPHeaders(headers)

    # Load cookies and convert them to the required format
    cookies = []

    # get existing cookies
    for cookie in browser_cookie3.load(domain_name='.facebook.com'):

        if cookie:
            cookie_dict = {
                'name': cookie.name,
                'value': cookie.value,
                'domain': cookie.domain,
                'path': cookie.path,
            }
            cookies.append(cookie_dict)

    await page.setCookie(*cookies)

    with open(csv_file, 'a', newline='', encoding='utf-8') as file:
        writer = csv.writer(file)

        # create header for csv file
        writer.writerow(['Post Owner', 'Text', 'Shares', 'Comments', 'Reaction', 'Post ID'])

        for keyword in keywords:
            # go to facebook url
            await page.goto('https://m.facebook.com/search/posts/?q=' + keyword + '&filters=eyJyZWNlbnRfcG9zdHM6MCI6IntcIm5hbWVcIjpcInJlY2VudF9wb3N0c1wiLFwiYXJnc1wiOlwiXCJ9In0%3D')

            # Scroll the page until the bottom
            while True:
                # Scroll to the bottom of the page
                await page.evaluate('window.scrollTo(0, document.body.scrollHeight)')

                # Wait for a short interval to allow the page to load new content (if necessary)
                await asyncio.sleep(time)  # Adjust the interval as needed

                # Check if the page has reached the bottom
                at_bottom = await page.evaluate('(window.innerHeight + window.scrollY) >= document.body.offsetHeight')
                if at_bottom:
                    break

            # takes a screenshot of the page
            # await page.screenshot({'path': 'proof.png'})

            # Get the HTML content of the scrolled page
            html_content = await page.content()

            soup = BeautifulSoup(html_content, 'html.parser')

            # Get all posts
            posts = soup.find_all('div', class_="_a5o _9_7 _2rgt _1j-f")

            # Iterate over the found div elements

            for post in posts:
                owner = ""
                text = ""
                number_of_shares = ""

                # Find the <a> element with the specified class names
                strong_elements_inside_text_with_attributes = post.find_all('strong')
                if strong_elements_inside_text_with_attributes:
                    a_elements_inside_text_with_attributes = strong_elements_inside_text_with_attributes[0].find('a')
                    owner = a_elements_inside_text_with_attributes.get_text(
                        strip=True) if a_elements_inside_text_with_attributes else ""
                else:
                    owner = ""

                # Get text and shares
                p_elements_inside_text_with_attributes = ""
                span_elements_inside_text_with_attributes = ""

                before_text_with_attributes = post.find('div', class_="_5rgt _5nk5 _5msi")

                # Get post_ID
                part_of_url = 'fbid'
                post_id_element = before_text_with_attributes.find('a', href=lambda
                    href: href and part_of_url in href) if before_text_with_attributes else None

                if post_id_element:
                    post_id = post_id_element.get('href').split("=")[1].split("&")[0]
                else:
                    post_id = ""

                if before_text_with_attributes:
                    text_with_attributes = before_text_with_attributes.find_all('span')
                else:
                    text_with_attributes = []

                for find_text in text_with_attributes:
                    if len(p_elements_inside_text_with_attributes) == 0:
                        p_elements_inside_text_with_attributes = find_text.find_all('p')
                    if find_text.find('span', class_='text_exposed_show'):
                        span_elements_inside_text_with_attributes = find_text.find('span',
                                                                                   class_='text_exposed_show').get_text(
                            strip=True)

                for index, p_element in enumerate(p_elements_inside_text_with_attributes):
                    if text != "":
                        text += "\n" + p_element.get_text(strip=True)
                    else:
                        text = p_element.get_text(strip=True)
                    if index == len(p_elements_inside_text_with_attributes) - 1:
                        text += "\n" + span_elements_inside_text_with_attributes

                number_of_shares = ""
                number_of_comment = ""
                number_of_reaction = ""
                find_shares = post.find('div', class_="_34qc _3hxn _3myz _4b45")
                comments_and_shares = find_shares.find('div', class_="_1fnt") if find_shares else None
                # Iterate over each <span> tag
                if comments_and_shares:
                    # Check if the text content contains the word "share"
                    for div in comments_and_shares:
                        if 'comment' in div.text:
                            number_of_comment = div.text
                        if 'share' in div.text:
                            number_of_shares = div.text

                find_reaction = post.find('div', class_="_1g06")
                if find_reaction:
                    number_of_reaction = find_reaction.get_text(strip=True)

                writer.writerow([owner, text, number_of_shares, number_of_comment, number_of_reaction, post_id])

    # close browser
    await browser.close()


def search(keywords, time, csv_file):
    #nest_asyncio.apply()
    asyncio.run(search_fb(keywords, time, csv_file))
